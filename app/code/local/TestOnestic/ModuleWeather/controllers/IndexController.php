<?php
 
class TestOnestic_ModuleWeather_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout();

        $block = $this->getLayout()->createBlock("core/text","testonestic_moduleweather/content");
        $block->setText("
        <input type='text' id='location' placeholder='Digite o nome da cidade que viocê está'/>
        <button onclick='consultWeather()'>Consultar</button>
        <div class='panelWeather'>
            <p><span id='weather'> </span></p>
            <p><span id='wind'> </span></p>
            <p><span id='temperature'> </span></p>
            <p><span id='cloud'> </span></p>
            <p><span id='humidity'> </span></p>
            <p><span id='pressure'> </span></p>
        </div>
        <script src='https://unpkg.com/axios/dist/axios.min.js'></script>
        <script>
        function consultWeather() {
            let locationValue = document.querySelector('#location').value;

            const params = {
            access_key: 'f511be119aa4f52da28cd1a0f7df926d',
            query: locationValue
            };

            axios
            .get('http://api.weatherstack.com/current', { params })
            .then(response => {
                if (!response.data.error) {
                const apiResponse = response.data;
                document.querySelector(
                    '#weather'
                ).innerHTML = 'Weather:' + apiResponse.current.weather_descriptions;
                document.querySelector(
                    '#wind'
                ).innerHTML = 'Wind Speed:' + apiResponse.current.wind_speed +' <br/> Wind Degree:' + apiResponse.current.wind_degree + ' <br/> Wind Direction:' + apiResponse.current.wind_dir;
                document.querySelector(
                    '#temperature'
                ).innerHTML = 'Temperature:' + apiResponse.current.temperature+ '℃';
                document.querySelector(
                    '#cloud'
                ).innerHTML = 'Cloud Cover:' + apiResponse.current.cloudcover;
                document.querySelector(
                    '#humidity'
                ).innerHTML = 'Humidity:' + apiResponse.current.humidity;
                document.querySelector(
                    '#pressure'
                ).innerHTML = 'Pressure:' + apiResponse.current.pressure;
                } else {
                console.log('Error: code:' + response.data.error.code +', info:' + response.data.error.info);
                }
            })
            .catch(error => {
                console.error('Error: ', error);
            });
        }
        </script>
        ");
        

        echo "<script>
        function consultWeather() {
        let locationValue = document.querySelector('#location').value;
        fetch('http://api.weatherstack.com/current?access_key=f511be119aa4f52da28cd1a0f7df926d&query='+locationValue)
            .then(response => response.json())
            .then(data => console.log('data is', data))
            .catch(error => console.log('error is', error));
        }
        </script>";

        $this->getLayout()->getBlock("content")->insert($block);
        $this->renderLayout();

        
    }
}